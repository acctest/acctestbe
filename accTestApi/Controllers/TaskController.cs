﻿using accTestApi.helper;
using accTestApi.Model;
using accTestApi.Services.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace accTestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITasks _iInterface;

        public TaskController(ITasks itasks)
        {
            _iInterface = itasks;
        }

        [HttpGet("GetTasks")]
        public async Task<IActionResult> GetTasks([BindRequired] int UserId)
        {
            try
            {
                return Ok(await _iInterface.GetTasks(UserId));
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    Succes = false,
                    Message = ex.Message,
                    Rows = 0,
                    Data = null
                });
            }
        }

        [HttpPost("AddTask")]
        public async Task<IActionResult> AddTask([FromBody] TaskParam payload)
        {
            try
            {
                return Ok(await _iInterface.AddTask(payload));
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    Succes = false,
                    Message = ex.Message,
                    Rows = 0,
                    Data = null
                });
            }
        }

        [HttpPost("DeleteTask")]
        public async Task<IActionResult> DeleteTask([FromBody] TaskParamId payload)
        {
            try
            {
                return Ok(await _iInterface.DeleteTask(payload.TaskId));
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    Succes = false,
                    Message = ex.Message,
                    Rows = 0,
                    Data = null
                });
            }
        }

        [HttpPost("CompleteTask")]
        public async Task<IActionResult> CompleteTask([FromBody] TaskParamId payload)
        {
            try
            {
                return Ok(await _iInterface.CompleteTask(payload.TaskId));
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    Succes = false,
                    Message = ex.Message,
                    Rows = 0,
                    Data = null
                });
            }
        }

        [HttpPost("UnCompleteTask")]
        public async Task<IActionResult> UnCompleteTask([FromBody] TaskParamId payload)
        {
            try
            {
                return Ok(await _iInterface.UnCompleteTask(payload.TaskId));
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    Succes = false,
                    Message = ex.Message,
                    Rows = 0,
                    Data = null
                });
            }
        }
    }
}
