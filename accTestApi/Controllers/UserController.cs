﻿using accTestApi.helper;
using accTestApi.Model;
using accTestApi.Services.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace accTestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUser _iInterface;

        public UserController(IUser iUser)
        {
            _iInterface = iUser;
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] UserParam payload)
        {
            try
            {
                return Ok(await _iInterface.Login(payload));
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    Succes = false,
                    Message = ex.Message,
                    Rows = 0,
                    Data = null
                });
            }
        }

        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody] UserParam payload)
        {
            try
            {
                return Ok(await _iInterface.Register(payload));
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    Succes = false,
                    Message = ex.Message,
                    Rows = 0,
                    Data = null
                });
            }
        }
    }
}
