﻿using accTestApi.helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace accTestApi.Model
{
    public class TasksModel
    {
        public int TaskId { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public bool? IsCompleted { get; set; }

    }
    public class TaskParam
    {
        public int TaskId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public string DueDateStr { get
            {
                if (DueDate != null)
                    return DateTimeHelper.ToISODateTimeString(DueDate);
                else
                    return "";
            } 
        }
        public bool IsCompleted { get; set; } = false;
    }
    public class TaskParamId
    {
        [Required]
        public int TaskId { get; set; }
    }
}
