﻿using accTestApi.helper;
using accTestApi.Model;
using System.Threading.Tasks;

namespace accTestApi.Services.User
{
    public interface IUser
    {
        Task<BaseResponse> Login(UserParam payload);
        Task<BaseResponse> Register(UserParam payload);
    }
}
