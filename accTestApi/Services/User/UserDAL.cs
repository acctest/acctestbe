﻿using accTestApi.helper;
using accTestApi.helper.Dapper;
using accTestApi.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace accTestApi.Services.User
{
    public class UserDAL : IUser
    {
        private readonly IDapper _dapper;
        public UserDAL(IDapper dapper)
        {
            _dapper = dapper;
        }

        //public async Task<BaseResponse> MethodName(UserParam payload)
        //{
        //    BaseResponse result = new BaseResponse()
        //    try
        //    {

        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return result;
        //}

        public async Task<BaseResponse> Login(UserParam payload)
        {
            BaseResponse result = new BaseResponse();
            try
            {
                string userName = payload.Username;

                string sql = $@"SELECT UserId, UserName, PasswordHash FROM dbo.Users WHERE Username = '{userName}';";
                List<Users> listData = await Task.FromResult(_dapper.GetAll<Users>(sql));
                Users resultData = listData.Where(o => BCrypt.Net.BCrypt.Verify(payload.PasswordHash, o.PasswordHash)).FirstOrDefault();
                if (resultData != null)
                {
                    resultData.PasswordHash = "";
                    result.Data = resultData;
                    result.Succes = true;
                    result.Rows = 1;
                }
                else
                {
                    result.Succes = false;
                    result.Message = "Please Check User Name and Password.";
                }
            }
            catch
            {
                throw;
            }
            return result;
        }

        public async Task<BaseResponse> Register(UserParam payload)
        {
            BaseResponse result = new BaseResponse();
            try
            {
                string userName = payload.Username;
                string pw = BCrypt.Net.BCrypt.HashPassword(payload.PasswordHash);

                string sql = $@"INSERT INTO dbo.Users(Username, PasswordHash) VALUES('{userName}', '{pw}');
                                SELECT SCOPE_IDENTITY(); ";
                int identity = await Task.FromResult(_dapper.Get<int>(sql));

                sql = $@"SELECT UserId, UserName FROM dbo.Users WHERE UserId = {identity}; ";
                var resultData = await Task.FromResult(_dapper.Get<Users>(sql));
                if (resultData != null)
                {
                    result.Data = resultData;
                    result.Succes = true;
                    result.Rows = 1;
                }
                else
                {
                    result.Succes = false;
                    result.Message = "Please Check User Name and Password.";
                }
            }
            catch
            {
                throw;
            }
            return result;
        }

    }
}
