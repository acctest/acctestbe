﻿using accTestApi.helper;
using accTestApi.Model;
using System.Threading.Tasks;

namespace accTestApi.Services.Tasks
{
    public interface ITasks
    {
        Task<BaseResponse> AddTask(TaskParam payload);
        Task<BaseResponse> GetTasks(int userId);
        Task<BaseResponse> DeleteTask(int TaskId);
        Task<BaseResponse> CompleteTask(int TaskId);
        Task<BaseResponse> UnCompleteTask(int TaskId);

    }
}
