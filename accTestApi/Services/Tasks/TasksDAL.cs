﻿using accTestApi.helper;
using accTestApi.helper.Dapper;
using accTestApi.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace accTestApi.Services.Tasks
{
    public class TasksDAL : ITasks
    {
        private readonly IDapper _dapper;

        public TasksDAL(IDapper dapper)
        {
            _dapper = dapper;
        }

        public async Task<BaseResponse> AddTask(TaskParam payload)
        {
            BaseResponse result = new BaseResponse();
            try
            {
                string sql = "";
                if (payload.TaskId > 0)
                {
                    sql = $@" UPDATE dbo.Tasks SET 
                            Title = '{payload.Title}'
                            ,Description = '{payload.Description}'
                            ,DueDate = '{payload.DueDateStr}'
                            ,IsCompleted = '{payload.IsCompleted}'
                            WHERE TaskId = 0; ";
                }
                sql = $@" INSERT INTO dbo.Tasks
                                (UserId
                                ,Title
                                ,Description
                                ,DueDate
                                ,IsCompleted)
                                VALUES
                                ({payload.UserId}
                                ,'{payload.Title}'
                                ,'{payload.Description}'
                                ,'{payload.DueDateStr}'
                                ,'{payload.IsCompleted}'
                                ); ";
                int affected = await Task.FromResult(_dapper.Execute(sql));
                if(affected > 0)
                {
                    result.Data = new { AffectedRows = affected };
                    result.Succes = true;
                }
                else
                {
                    result.Succes = false;
                    result.Message = "0 rows inserted";
                }
            }
            catch
            {
                throw;
            }
            return result;
        }
        public async Task<BaseResponse> GetTasks(int userId)
        {
            BaseResponse result = new BaseResponse();
            try
            {
                string sql = $@"SELECT * FROM dbo.Tasks WHERE UserId = {userId}";
                List<TasksModel> resultData = new List<TasksModel>();
                resultData = await Task.FromResult(_dapper.GetAll<TasksModel>(sql));

                if (resultData.Count > 0)
                {
                    result.Data = resultData;
                    result.Succes = true;
                }
                else
                {
                    result.Succes = false;
                    result.Message = "data is undefined";
                }
            }
            catch
            {
                throw;
            }
            return result;
        }
        public async Task<BaseResponse> DeleteTask(int TaskId)
        {
            BaseResponse result = new BaseResponse();
            try
            {
                string sql = $@"DELETE FROM dbo.Tasks WHERE TaskId = {TaskId}";
                List<TasksModel> resultData = new List<TasksModel>();
                int affected = await Task.FromResult(_dapper.Execute(sql));
                if (affected > 0)
                {
                    result.Data = new { AffectedRows = affected };
                    result.Succes = true;
                    result.Message = $" task Id {TaskId} deleted.";
                }
                else
                {
                    result.Succes = false;
                    result.Message = "0 Row Deleted";
                }
            }
            catch
            {
                throw;
            }
            return result;
        }
        
        public async Task<BaseResponse> CompleteTask(int TaskId)
        {
            BaseResponse result = new BaseResponse();
            try
            {
                string sql = $@"UPDATE dbo.Tasks SET IsCompleted = 1 WHERE TaskId = {TaskId}";
                List<TasksModel> resultData = new List<TasksModel>();
                int affected = await Task.FromResult(_dapper.Execute(sql));
                if (affected > 0)
                {
                    result.Data = new { AffectedRows = affected };
                    result.Succes = true;
                    result.Message = $" task Id {TaskId} deleted.";
                }
                else
                {
                    result.Succes = false;
                    result.Message = "0 Row Deleted";
                }
            }
            catch
            {
                throw;
            }
            return result;
        }

        public async Task<BaseResponse> UnCompleteTask(int TaskId)
        {
            BaseResponse result = new BaseResponse();
            try
            {
                string sql = $@"UPDATE dbo.Tasks SET IsCompleted = 0 WHERE TaskId = {TaskId}";
                List<TasksModel> resultData = new List<TasksModel>();
                int affected = await Task.FromResult(_dapper.Execute(sql));
                if (affected > 0)
                {
                    result.Data = new { AffectedRows = affected };
                    result.Succes = true;
                    result.Message = $" task Id {TaskId} deleted.";
                }
                else
                {
                    result.Succes = false;
                    result.Message = "0 Row Deleted";
                }
            }
            catch
            {
                throw;
            }
            return result;
        }
    }
}
