﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace accTestApi.helper
{
    public class BaseResponse
    {
        public bool Succes { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public int Rows { get; set; }
    }
}
