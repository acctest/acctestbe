﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;

namespace accTestApi.helper.Dapper
{
    public interface IDapper : IDisposable
    {
        int CountAll(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        int CountAll<T>(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        T Get<T>(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        List<T> GetAll<T>(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        T[] GetArray<T>(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        IEnumerable<T> GetEnumerable<T>(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        int Execute(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        T Insert<T>(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        T Update<T>(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
    }
}
