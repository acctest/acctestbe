﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace accTestApi.helper
{
    public static class DateTimeHelper
    {

        public static DateTime SQLServerMinValue
        {
            get
            {
                return new DateTime(1753, 1, 1);
            }
        }

        public static string ToDefaultDate(this DateTime dateTime)
        {
            return dateTime.ToString("dd MMM yyyy");
        }

        public static string ToDefaultDateTime(this DateTime dateTime)
        {
            return dateTime.ToString("dd MMM yyyy HH:mm");
        }

        public static string ToDefaultShortDate(this DateTime dateTime)
        {
            return dateTime.ToString("dd-MMM-yy");
        }

        public static string ToISODateTimeString(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-ddTHH:mm:ss");
        }

        public static string ToISODateString(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd");
        }

        #region Convertion Time Only

        public static string ToDefaultTime(this DateTime dateTime)
        {
            return dateTime.ToString("hh:mm:ss");
        }

        #endregion
    }
}
