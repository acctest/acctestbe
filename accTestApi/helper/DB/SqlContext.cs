﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace accTestApi.helper.DB
{
    public class SqlContext
    {
        public string ConnectionString { get; set; }

        public SqlContext()
        {
            string connectionString = DBConnection.GetConnstring();
            this.ConnectionString = connectionString;
        }

        public SqlConnection GetConnection()
        {
            return new SqlConnection(ConnectionString);
        }
    }
}
