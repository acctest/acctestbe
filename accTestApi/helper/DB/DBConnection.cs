﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace accTestApi.helper.DB
{
    public class DBConnection
    {
        // Load DB from appsetting.json
        public static string GetConnstring()
        {
            var FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "")).Root;
            var Connstring = new ConfigurationBuilder().AddJsonFile(FileProvider + "appsettings.json").Build().GetSection("ConnectionStrings");
            return Connstring["Database.Host"];

            //var MysqlConnstring = "server=localhost; uid= root; pwd=BorlandDelph1; database=yilz_sso";
            //return MysqlConnstring;
        }
    }
}
